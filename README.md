# Mnożenie skalarne wektorów na CPU i GPU

Program wykonuje mnożenie skalarne dwóch wektorów, sekwencyjnie oraz równolegle na karcie graficznej.
Na wyjściu programu otrzymujemy pomiary czasu obu procesów wraz z wynikami.

### Sekwencyjnie, funkcja `count_sequence_scalar_product(float *v1, float *v2)`

Mnożenie kolejno składowych wektorów i zapisaniu ich sumy w zmiennej, która zostaje zwrócona przez funkcję.

	sum += hostA[i] * hostB[i];

### Równolegle na GPU

Mnożenie odbywa się za pomocą kernela `multiply_vectors(float *devA, float *devB, float *devC)`.
Sumowanie wyników z wektora devC jest obliczane za pomocą [redukcji][1].

### Uruchomienie

	$ make
	$ run

### Przykładowe wywołania

##### Rozmiar wektorów: 2097152
##### Ilość watków na blok: 64

#### float

	Time sequence: 0.0102085610
	Time parallel: 0.0099691440
	Sequence result: 52396464.00
	Parallel result: 52430504.00

#### int

	Time sequence: 0.0103683570
	Time parallel: 0.0099638350
	Sequence result: 42536786
	Parallel result: 42536786


[1]: http://piotao.inf.ug.edu.pl/zonkg/wyklady/reduction.pdf
