#include <stdlib.h>
#include <stdio.h>
#include "lib/timers.h"
#define N 1024*1024*2
#define THREADS_PER_BLOCK 64

__global__ void multiply_vectors(float *devA, float *devB, float *devC) {
  int i = blockIdx.x*blockDim.x + threadIdx.x;
  devC[i] = devA[i] * devB[i];
}
  
  // sequential addressing
__global__ void reduce(float *devC) {
  __shared__ float sdata[THREADS_PER_BLOCK];
  
  // each thread loads one element from global to shared mem
  unsigned int tid = threadIdx.x;
  unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
  sdata[tid] = devC[i];
  __syncthreads();

  for (unsigned int s=blockDim.x/2; s>0; s>>=1) {
    if (tid < s) {
      sdata[tid] += sdata[tid + s];
    }
    __syncthreads();
  }

  // write result for this block to global mem
  if (tid == 0) devC[blockIdx.x] = sdata[0];
}

void set(float *v){
  for (int i=0; i<N; i++){
    v[i] = (float)rand()/((float)RAND_MAX/10); // random number [0-10]
  }
} 

float count_sequence_scalar_product(float *hostA, float *hostB){
  float sum = 0.0;
  for(int i=0; i<N; i++) {
    sum += hostA[i] * hostB[i];
  }
  return sum;
}

int main(){
  int threads_per_block = THREADS_PER_BLOCK;
  float size = N * sizeof(float);
  pTimer sequence_clock = newTimer(); pTimer parallel_clock = newTimer();
  float sequence_scalar_product = 0.0;
  float *hostA, *hostB, *hostC;
  float *devA, *devB, *devC;
  srand((unsigned)time(0));

  // DEVICE
  cudaMalloc((void**)&devA, size);
  cudaMalloc((void**)&devB, size);
  cudaMalloc((void**)&devC, size);
    
  // HOST
  hostC = (float *)malloc(size);
  hostA = (float *)malloc(size); set(hostA);
  hostB = (float *)malloc(size); set(hostB);

  // COPY TO DEVICE
  cudaMemcpy(devA, hostA, size, cudaMemcpyHostToDevice);
  cudaMemcpy(devB, hostB, size, cudaMemcpyHostToDevice);
  
  // PARALLEL COMPUTATIONS
  startTimer(parallel_clock);
  multiply_vectors<<<N/threads_per_block, threads_per_block>>>(devA, devB, devC);

  for(int i=N/threads_per_block; i>=1 && threads_per_block>1; i/=threads_per_block){

    reduce<<<i,threads_per_block>>>(devC);
    
    // PRINT REDUCE PROCESS
    // printf("\n\nfrom %d blocks, %d threads per block\n", i, threads_per_block);
    // cudaMemcpy(hostC, devC, size, cudaMemcpyDeviceToHost);
    // for (int k=0; k<i; k++)
    //   printf("%.2f ", hostC[k]);
    
    while((i%threads_per_block) > 0)
      threads_per_block /= 2;
  }

  cudaMemcpy(hostC, devC, size, cudaMemcpyDeviceToHost);
  stopTimer(parallel_clock);

  // SEQUENCE COMPUTATIONS
  startTimer(sequence_clock);
  sequence_scalar_product = count_sequence_scalar_product(hostA, hostB);
  stopTimer(sequence_clock);

  // PRINT RESULTS
  printTimerFmt("\nTime sequence: %2.10f\n", sequence_clock);
  printTimerFmt("Time parallel: %2.10f\n", parallel_clock);
  printf("Sequence result: %.2f\n", sequence_scalar_product);
  printf("Parallel result: %.2f\n\n", hostC[0]);

  // MAKE MEMORY FREE
  freeTimer(sequence_clock); freeTimer(parallel_clock);
  free(hostC); free(hostA); free(hostB);
  cudaFree(devC); cudaFree(devA); cudaFree(devB);
  return 0;
}