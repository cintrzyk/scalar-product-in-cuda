NVCC=nvcc
CFLAGS=--compiler-options "-Wall"

run: scalar_product.cu
	$(NVCC) $(CFLAGS) scalar_product.cu -o run